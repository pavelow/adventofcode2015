#! /usr/bin/env python

data = []
with open("input.txt") as f:
    data = f.read()

floor = 0
for idx, i in enumerate(data, 1):
    floor -= 1
    if i == '(': floor += 2
    if floor < 0:
        print(idx)
        break
