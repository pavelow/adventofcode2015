#! /usr/bin/env python

data = []
with open("input.txt") as f:
    data = f.read()

print(data.count('(') - data.count(')'))

