#! /usr/bin/env python
import math

data = []
with open("input.txt") as f:
    data = f.read().split('\n')[:-1]

for i in range(len(data)):
    data[i] = list(map(int, data[i].split('x')))

total = 0
for p in data:
    p.sort()
    total += 2*p[0]+2*p[1] + math.prod(p)
print(total)
