#! /usr/bin/env python

data = []
with open("input.txt") as f:
    data = f.read().split('\n')[:-1]

for i in range(len(data)):
    data[i] = list(map(int, data[i].split('x')))

total = 0
for p in data:
    sides = []
    sides.append(2*p[0]*p[1])
    sides.append(2*p[1]*p[2])
    sides.append(2*p[0]*p[2])
    total += sum(sides) + min(sides)//2
print(total)
